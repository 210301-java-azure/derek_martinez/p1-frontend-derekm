document.getElementById("registration-form").addEventListener("submit", addNewUser);

function addNewUser(e){
    e.preventDefault();
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    const email = document.getElementById("inputEmail").value;
    ajaxRegister(username, password, email, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "account created, please login: " + "<a href='login.html'>here</a>";
}

function indicateFailure(){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "your account could not be created";
}