const token = sessionStorage.getItem("token");
const userId = sessionStorage.getItem("User-Id");
const urlString = window.location.search;

let postId = getPostId(urlString);

function getPostId(urlString){
    let numStr = urlString.replace(/[^0-9]/g,'');
    return numStr;
}

getPostDetails(postId);
getComments(postId);

function getPostDetails(postId){
    
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open("GET", `http://13.90.251.61/posts/${postId}`);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                const postObj = JSON.parse(xhr.responseText);
                displayPostBody(postObj);
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send();

}

function getComments(postId){
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open("GET", `http://13.90.251.61/posts/${postId}/comments`);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                const commentsObj = JSON.parse(xhr.responseText);
                generateComments(commentsObj);
            } else {
                console.log("something went wrong with your request")
            }
        }
    }
    xhr.send();
}

function displayPostBody(postObj){
    let body = document.getElementById("postBody");
    let title = document.getElementById("postTitle");
    let byAndDate = document.getElementById("postAuthorAndDate");

    body.innerHTML = postObj.postBody;
    title.innerHTML = postObj.title;
    byAndDate.innerHTML = "By " + `<a style=color:white; href=user-posts.html?userId=${postObj.user.userId}>` + postObj.user.username + "</a>" + " on " + postObj.dateCreated;

    if(userId == postObj.user.userId){
        document.getElementById("edit-post-button").style.display = "initial";
        document.getElementById("delete-post-button").style.display = "initial";
    }
    
}

if(token){

    document.getElementById("login-nav").style.display = "none";
    document.getElementById("register-nav").style.display = "none";
    document.getElementById("logout-nav").style.display = "initial";
    document.getElementById("welcome-nav").style.display = "initial";
    document.getElementById("logout-nav").style.display = "initial";
    document.getElementById("add-comment-button").style.display = "initial";
    
  
    const xhr = new XMLHttpRequest();
    xhr.open("GET", `http://13.90.251.61/users/${userId}`);
    xhr.setRequestHeader("Authorization",token);
    xhr.setRequestHeader("User-Id", userId);
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status == 200){
                const user = JSON.parse(xhr.responseText);
                helloUser(user);
            } else{
                console.log("something went wrong with your request");
            }
            
        }
    }
    xhr.send();
}

document.getElementById("logout-nav").addEventListener("click", logout);

function logout(){
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("User-Id");

    document.getElementById("login-nav").style.display = "initial";
    document.getElementById("register-nav").style.display = "initial";
    document.getElementById("logout-nav").style.display = "none";
    document.getElementById("create-post-button").style.display ="none";
    document.getElementById("welcome-nav").style.display = "none";
}

function helloUser(user){
    document.getElementById("welcome-nav").innerHTML = "Hello, " + user.username;
}

document.getElementById("submit-edit").addEventListener("submit", editPost);

function editPost(e){
    e.preventDefault();
    const newTitle = document.getElementById("edit-title").value;
    const newPostBody = document.getElementById("edit-body").value;
    const updatedPost = {"title": newTitle, "postBody":newPostBody};
    ajaxEditPost(updatedPost, postId, editPostSuccess, editPostFailure);
}

function editPostSuccess(){
    console.log("success");
    window.location.href = `./post-details.html?postId=${postId}`;

}

function editPostFailure(){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "your post could not be updated";
}


document.getElementById("confirm-delete-post-button").addEventListener("click", deletePost);

function deletePost(e){
    e.preventDefault();
    ajaxDeletePost(postId, deletePostSuccess, deletePostFailure);
}

function deletePostSuccess(){
    console.log("success");
    window.location.href = './index.html';

}

function deletePostFailure(){
    const message = document.getElementById("create-msg");
    message.hidden = false;
    message.innerHTML = "your post could not be updated";
}

function generateComments(comments){
    for(let i = 0; i < comments.length; i++){
        let newCommentContainer = document.createElement("div");
        newCommentContainer.id = "comment-container";
        newCommentContainer.className = "w3-panel ";
        newCommentContainer.className += "w3-large";
        newCommentContainer.innerHTML = `<p id="comment-body" class="comment-body">` + comments[i].commentBody + "</p>";

        let commentDetails = document.createElement("div");
        commentDetails.id = "comment-author-container";
        commentDetails.innerHTML = `<p>By <a href="user-posts.html?userId=${comments[i].user.userId}" style="color:#80cbc4">` + comments[i].user.username + "</a> on " + comments[i].dateCreated + "</p>";
        
        document.getElementById("comment-section").insertBefore(commentDetails, document.getElementById("comment-section").childNodes[0]);
        document.getElementById("comment-section").insertBefore(newCommentContainer, document.getElementById("comment-section").childNodes[0]);
        
    }
}
