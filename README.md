# p1 - Message Board Design and Functionality - Derek M.

[Message Board Home](http://derekmp1.blob.core.windows.net/p1-frontend/index.html)

### Info
View posts and comments made by users. Creating custom posts and comments require a user account. Users can edit and delete their own posts.

### Pages  
#### `/index.html` 
The site landing page. Users posts are presented publically as a colorful bar. Clicking on the post title here will take you to the contents of that post. 

Logged in users have access to a `New Post +` button which will allow them to create and add a post to the current list displayed on the index. Newer posts appear at the top of the page. 

---

#### `/register.html`  
Used to great a new user account. After successful account creation a modul will notify user of their registration and provide a link to login.

---

#### `/login.html`  
Used for authenticating registered users. Upon successful login, an http response will provide an authorization token and the user-id for session storage.

---

### `/create-new-post.html`  
Page for creating a new post and adding that post to the index. After filling in the required fields a modul will alert the user if their post could be created or not.

---

### `/user-posts.html?userId=${userid}`  
Entering a valid user ID into the URL will display all posts associated with that ID.

---

### `/post-details.html?postId=${postid}`  
Entering a valid post ID will display the all content related to that post ID. Registered users will have access to `Add Comment +` where they can add a comment to the post. If the post belongs to the user they will have additional access to `Edit Post` and `Delete Post` functionality.

---

### Contributions
Thanks to Carolyn and all my Revature cohorts who assisted in help making this project possible!
